namespace Gameready.Helpers.SpriteAtlasLateBinding
{
    public interface ISpriteAtlasLateBinding
    {
        void UnloadLoadedAtlases();

        void UnloadLoadedAtlases(params string[] atlasNames);

        bool IsAtlasLoaded(params string[] atlasNames);

        bool TryPreloadAtlases(params string[] atlasNames);
    }
}