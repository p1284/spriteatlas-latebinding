using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Gameready.Helpers.SpriteAtlasLateBinding
{
    [CreateAssetMenu(fileName = "SpriteAtlasLateBindingConfig",menuName = "Tools/Configs/SpriteAtlasLateBindingConfig")]
    public class SpriteAtlasLateBindingConfig:ScriptableObject
    {
        [Serializable]
        private class AtlasReferenceData
        {
            public string AtlasName;
            public AssetReference excludeFromBuildAtlas;
        }

        [SerializeField] private AtlasReferenceData[] _atlasesToBind;
        
        
        public AssetReference FindSpriteAtlasReference(string atlasName)
        {
            for (int i = 0; i < _atlasesToBind.Length; i++)
            {
                if (_atlasesToBind[i].AtlasName == atlasName)
                {
                    return _atlasesToBind[i].excludeFromBuildAtlas;
                }
            }
        
            return null;
        }
    }
}