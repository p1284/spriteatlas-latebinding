using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;

namespace Gameready.Helpers.SpriteAtlasLateBinding
{
    /// <summary>
    /// https://docs.unity3d.com/2017.4/Documentation/Manual/SpriteAtlasDistribution.html#Dontinclbuild
    /// https://support.unity.com/hc/en-us/articles/360000665546-Sprite-Atlas-with-late-binding-and-Asset-Bundles
    /// </summary>
    public sealed class SpriteAtlasLateBindingManager : MonoBehaviour, ISpriteAtlasLateBinding
    {
        [SerializeField] private SpriteAtlasLateBindingConfig _config;

        private Dictionary<string, Action<SpriteAtlas>> _registerCallbacks;
        private Dictionary<string, AssetReference> _loadedAtlases;

        void Awake()
        {
            _registerCallbacks = new Dictionary<string, Action<SpriteAtlas>>();
            _loadedAtlases = new Dictionary<string, AssetReference>();
        }

        private void OnEnable()
        {
            SpriteAtlasManager.atlasRegistered += OnAtlasRegisteredHandler;
            SpriteAtlasManager.atlasRequested += OnAtlasRequestedHandler;
        }

        private void OnDisable()
        {
            SpriteAtlasManager.atlasRegistered -= OnAtlasRegisteredHandler;
            SpriteAtlasManager.atlasRequested -= OnAtlasRequestedHandler;
        }

        public void UnloadLoadedAtlases()
        {
            foreach (var loadedAtlas in _loadedAtlases)
            {
                if (loadedAtlas.Value.IsValid())
                {
                    loadedAtlas.Value.ReleaseAsset();
                }
            }

            _loadedAtlases.Clear();
        }

        public void UnloadLoadedAtlases(params string[] atlasNames)
        {
            foreach (var atlasName in atlasNames)
            {
                if (_loadedAtlases.ContainsKey(atlasName))
                {
                    if (_loadedAtlases[atlasName].IsValid())
                    {
                        _loadedAtlases[atlasName].ReleaseAsset();
                        Debug.Log(
                            $"[SpriteAtlasLateBindingService] SpriteAtlas: {atlasName} unload complete!(Need load again)");
                    }

                    _loadedAtlases.Remove(atlasName);
                }
            }
        }

        public bool IsAtlasLoaded(params string[] atlasNames)
        {
            var count = 0;
            foreach (var atlasName in atlasNames)
            {
                count += _loadedAtlases.ContainsKey(atlasName) ? 1 : 0;
            }

            return count == atlasNames.Length || _loadedAtlases.Count == 0;
        }

        private void OnAtlasRegisteredHandler(SpriteAtlas spriteAtlas)
        {
            Debug.Log($"[SpriteAtlasLateBindingService] SpriteAtlas: {spriteAtlas.name} register complete!");
        }

        private void OnAtlasRequestedHandler(string atlasName, Action<SpriteAtlas> registerCallback)
        {
            if (!_registerCallbacks.ContainsKey(atlasName))
            {
                _registerCallbacks.Add(atlasName, registerCallback);
            }

            var atlasRef = _config.FindSpriteAtlasReference(atlasName);
            if (atlasRef == null)
            {
                Debug.LogError($"[SpriteAtlasLateBindingService] Binding atlas: {atlasName} not found!");
                return;
            }

            if (atlasRef.IsValid())
            {
                return;
            }

            StartCoroutine(PreloadAtlasRoutine(atlasRef));
        }

        public bool TryPreloadAtlases(params string[] atlasNames)
        {
            var atlasAssetsToPreload = new List<AssetReference>();
            for (int i = 0; i < atlasNames.Length; i++)
            {
                if (!_loadedAtlases.ContainsKey(atlasNames[i]) && _registerCallbacks.ContainsKey(atlasNames[i]))
                {
                    atlasAssetsToPreload.Add(_config.FindSpriteAtlasReference(atlasNames[i]));
                }
            }

            if (atlasAssetsToPreload.Count > 0)
            {
                StartCoroutine(PreloadAtlasesRoutine(atlasAssetsToPreload));
                return true;
            }

            return false;
        }

        private IEnumerator PreloadAtlasesRoutine(List<AssetReference> atlasAssetsToPreload)
        {
            foreach (var assetReference in atlasAssetsToPreload)
            {
                yield return PreloadAtlasRoutine(assetReference);
            }
        }

        private IEnumerator PreloadAtlasRoutine(AssetReference atlasRef)
        {
            if (atlasRef.IsValid())
            {
                yield break;
            }

            var handle = atlasRef.LoadAssetAsync<SpriteAtlas>();

            while (!handle.IsDone)
            {
                yield return null;
            }

            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                if (_registerCallbacks.ContainsKey(handle.Result.name))
                {
                    _registerCallbacks[handle.Result.name]?.Invoke(handle.Result);
                }

                _loadedAtlases.Add(handle.Result.name, atlasRef);
            }
        }
    }
}